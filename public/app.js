var ajaxCall = (function () {

  var data;

  function retrieveJSON(url, callback) {
    var httpRequest = new XMLHttpRequest();

    httpRequest.onreadystatechange = function() {
      if (httpRequest.readyState === 4) {
        if (httpRequest.status === 200) {
          data = JSON.parse(httpRequest.responseText);
          if(callback) {
            callback(data);
          }
        }
      }
    };

    httpRequest.open('GET', url);
    httpRequest.send();
  }

  function postAjax(url, data, success) {
    var params = typeof data == 'string' ? data : Object.keys(data).map(
          function(k){ return encodeURIComponent(k) + '=' + encodeURIComponent(data[k]) }
        ).join('&');

    var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
    xhr.open('POST', url);
    xhr.onreadystatechange = function() {
      if (xhr.readyState>3 && xhr.status==200) { success(xhr.responseText); }
    };
    xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.setRequestHeader('Content-length', params.length);
    xhr.send(params);
    return xhr;
  }

  return {
    retrieveJSON: retrieveJSON,
    postAjax: postAjax
  }
}());

var domHelper =(function() {

  function createElement(element) {
    return document.createElement(element);
  }

  function append(child, parent) {
    return parent.appendChild(child);
  }

  function findElement(selector) {
    return document.querySelector(selector);
  }

  function findElements(selector) {
    return document.querySelectorAll(selector);
  }

  function hasClass(element, className) {
      return element.classList ? element.classList.contains(className) : new RegExp('\\b'+ className+'\\b').test(element.className);
  }

  function addClass(element, className) {
    if (element.classList) {
      element.classList.add(className);
    } else if (!hasClass(element, className)) {
      element.className += ' ' + className;
    }
  }

  function removeClass(element, className) {
    if (element.classList) {
      element.classList.remove(className);
    } else {
      element.className = element.className.replace(new RegExp('\\b'+ className+'\\b', 'g'), '');
    }
  }

  function removeAttribute(element, attribute) {
    element.removeAttribute(attribute);
  }

  function addAttribute(element, attribute, value) {
    element.setAttribute(attribute, value);
  }

  return {
    createElement: createElement,
    append: append,
    findElement: findElement,
    findElements: findElements,
    addClass: addClass,
    removeClass: removeClass,
    hasClass: hasClass,
    removeAttribute: removeAttribute,
    addAttribute: addAttribute
  }

}());

var events = (function () {

  function addEvent(element, event, callback) {
    element.addEventListener(event, callback);
  }

  return {
    addEvent: addEvent
  }
}());

var starWars = (function () {
  
  var charactersToDisplay = 20,
      characterClass = 'character',
      selectedCharacterClass= 'selected',
      chosenCharacter = 'chosen-character',
      colQuarter = 'col-third',
      retrievedData,
      button,
      listElement,
      list,
      baseUrl;

  function dataReceived(data) {
    retrievedData = data;
    buildDomList(data);
  }

  function buildDomList(data) {
    listElement = domHelper.createElement('li');
    listElement.innerHTML = data.name;

    domHelper.addClass(listElement, characterClass);
    domHelper.addClass(listElement, colQuarter);
    domHelper.append(listElement, list);

    bindEvents(data, listElement);
  }

  function bindEvents(data, listElement) {
    events.addEvent(listElement, 'click', function(event) {
      event.preventDefault();
      selectCharacter(listElement);
      showSelectedData(data);
    });

    events.addEvent(button, 'click', function(event) {
      event.preventDefault();
      //ajaxCall.postAjax(url, data, success);
    })
  }

  function selectCharacter(listElement) {
    selectedElement = domHelper.findElement('.' + selectedCharacterClass);

    if (selectedElement === null) {
      domHelper.addClass(listElement, selectedCharacterClass);
      return;
    }

    domHelper.removeClass(selectedElement, selectedCharacterClass);
    domHelper.addClass(listElement, selectedCharacterClass);
  }

  function showSelectedData(data) {
    var displayContainer = domHelper.findElement('.' + chosenCharacter);

    displayContainer.innerHTML = '';
    domHelper.removeAttribute(button, 'disabled');

    nameElement = domHelper.createElement('p');
    eyesElement = domHelper.createElement('p');
    hairElement = domHelper.createElement('p');
    birthElement = domHelper.createElement('p');

    domHelper.append(nameElement, displayContainer);
    domHelper.append(eyesElement, displayContainer);
    domHelper.append(hairElement, displayContainer);
    domHelper.append(birthElement, displayContainer);

    nameElement.innerHTML = 'Name: ' + data.name;
    eyesElement.innerHTML = 'Hair color: ' + data.hair_color;
    hairElement.innerHTML = 'Eyes color: ' + data.eyes_color;
    birthElement.innerHTML = 'Birth Year:' + data.birth_year;
  }

  function init() {
    for (var i = 1; i <= charactersToDisplay; i++) {
      baseUrl = 'http://swapi.co/api/people/' + i + '/?format=json'
      ajaxCall.retrieveJSON(baseUrl, dataReceived);
    }

    list = domHelper.findElement('.list-wrapper');
    button = domHelper.findElement('button');

    domHelper.addAttribute(button, 'disabled', 'disabled');
  }



  return {
    init: init
  }
}());

starWars.init();
