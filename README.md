**Star Wars**

To get started you just need to open the index.html file or if you have node installed:

* npm install http-server -g
* http-server ./
* access localhost:8080

There are a few grunt tasks:

* grunt watch (watch only less files, could do js as well)
* grunt less (compile and minifies less files)
* grunt concat (concatanates js)
* grunt uglify (minifies, uglifies js)
* grunt js (will run grunt concat and uglify)
* grunt deploy (will run grunt less, concat and uglify)

What has not been done:

* filters (by gender, by hair color, etc.)
* no frameworks were used (Angular)