var events = (function () {

  function addEvent(element, event, callback) {
    element.addEventListener(event, callback);
  }

  return {
    addEvent: addEvent
  }
}());
