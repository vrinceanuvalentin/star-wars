var domHelper =(function() {

  function createElement(element) {
    return document.createElement(element);
  }

  function append(child, parent) {
    return parent.appendChild(child);
  }

  function findElement(selector) {
    return document.querySelector(selector);
  }

  function findElements(selector) {
    return document.querySelectorAll(selector);
  }

  function hasClass(element, className) {
      return element.classList ? element.classList.contains(className) : new RegExp('\\b'+ className+'\\b').test(element.className);
  }

  function addClass(element, className) {
    if (element.classList) {
      element.classList.add(className);
    } else if (!hasClass(element, className)) {
      element.className += ' ' + className;
    }
  }

  function removeClass(element, className) {
    if (element.classList) {
      element.classList.remove(className);
    } else {
      element.className = element.className.replace(new RegExp('\\b'+ className+'\\b', 'g'), '');
    }
  }

  function removeAttribute(element, attribute) {
    element.removeAttribute(attribute);
  }

  function addAttribute(element, attribute, value) {
    element.setAttribute(attribute, value);
  }

  return {
    createElement: createElement,
    append: append,
    findElement: findElement,
    findElements: findElements,
    addClass: addClass,
    removeClass: removeClass,
    hasClass: hasClass,
    removeAttribute: removeAttribute,
    addAttribute: addAttribute
  }

}());
