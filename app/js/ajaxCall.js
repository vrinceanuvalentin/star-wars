var ajaxCall = (function () {

  var data;

  function retrieveJSON(url, callback) {
    var httpRequest = new XMLHttpRequest();

    httpRequest.onreadystatechange = function() {
      if (httpRequest.readyState === 4) {
        if (httpRequest.status === 200) {
          data = JSON.parse(httpRequest.responseText);
          if(callback) {
            callback(data);
          }
        }
      }
    };

    httpRequest.open('GET', url);
    httpRequest.send();
  }

  function postAjax(url, data, success) {
    var params = typeof data == 'string' ? data : Object.keys(data).map(
          function(k){ return encodeURIComponent(k) + '=' + encodeURIComponent(data[k]) }
        ).join('&');

    var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
    xhr.open('POST', url);
    xhr.onreadystatechange = function() {
      if (xhr.readyState>3 && xhr.status==200) { success(xhr.responseText); }
    };
    xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.setRequestHeader('Content-length', params.length);
    xhr.send(params);
    return xhr;
  }

  return {
    retrieveJSON: retrieveJSON,
    postAjax: postAjax
  }
}());
