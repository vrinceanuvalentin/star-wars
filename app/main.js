var starWars = (function () {
  
  var charactersToDisplay = 20,
      characterClass = 'character',
      selectedCharacterClass= 'selected',
      chosenCharacter = 'chosen-character',
      colQuarter = 'col-third',
      retrievedData,
      button,
      listElement,
      list,
      baseUrl;

  function dataReceived(data) {
    retrievedData = data;
    buildDomList(data);
  }

  function buildDomList(data) {
    listElement = domHelper.createElement('li');
    listElement.innerHTML = data.name;

    domHelper.addClass(listElement, characterClass);
    domHelper.addClass(listElement, colQuarter);
    domHelper.append(listElement, list);

    bindEvents(data, listElement);
  }

  function bindEvents(data, listElement) {
    events.addEvent(listElement, 'click', function(event) {
      event.preventDefault();
      selectCharacter(listElement);
      showSelectedData(data);
    });

    events.addEvent(button, 'click', function(event) {
      event.preventDefault();
      //ajaxCall.postAjax(url, data, success);
    })
  }

  function selectCharacter(listElement) {
    selectedElement = domHelper.findElement('.' + selectedCharacterClass);

    if (selectedElement === null) {
      domHelper.addClass(listElement, selectedCharacterClass);
      return;
    }

    domHelper.removeClass(selectedElement, selectedCharacterClass);
    domHelper.addClass(listElement, selectedCharacterClass);
  }

  function showSelectedData(data) {
    var displayContainer = domHelper.findElement('.' + chosenCharacter);

    displayContainer.innerHTML = '';
    domHelper.removeAttribute(button, 'disabled');

    nameElement = domHelper.createElement('p');
    eyesElement = domHelper.createElement('p');
    hairElement = domHelper.createElement('p');
    birthElement = domHelper.createElement('p');

    domHelper.append(nameElement, displayContainer);
    domHelper.append(eyesElement, displayContainer);
    domHelper.append(hairElement, displayContainer);
    domHelper.append(birthElement, displayContainer);

    nameElement.innerHTML = 'Name: ' + data.name;
    eyesElement.innerHTML = 'Hair color: ' + data.hair_color;
    hairElement.innerHTML = 'Eyes color: ' + data.eyes_color;
    birthElement.innerHTML = 'Birth Year:' + data.birth_year;
  }

  function init() {
    for (var i = 1; i <= charactersToDisplay; i++) {
      baseUrl = 'http://swapi.co/api/people/' + i + '/?format=json'
      ajaxCall.retrieveJSON(baseUrl, dataReceived);
    }

    list = domHelper.findElement('.list-wrapper');
    button = domHelper.findElement('button');

    domHelper.addAttribute(button, 'disabled', 'disabled');
  }



  return {
    init: init
  }
}());

starWars.init();
