module.exports = function(grunt) {
  grunt.initConfig({
    // Less to css task
    less: {
      development: {
        options: {
          compress: true,
          yuicompress: true,
          optimization: 2
        },
        files: {
          "assets/css/style.css": "app/main.less"
        }
      }
    },

    // Watch tasks
    watch: {
      // Styles
      css: {
        files: ['app/**/*.less'],
        tasks: ['less'],
        options: {
          nospawn: true
        }
      },

      // Gruntconfig
      configFiles: {
        files: ['Gruntfile.js'],
        options: {
          reload: true
        }
      }
    },

    concat: {
      js: { //target
        src: ['./app/**/*.js'],
        dest: './public/app.js'
      }
    },

    uglify: {
      js: { //target
        src: ['./public/app.js'],
        dest: './public/min/app.js'
      }
   }
   
  });

  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');

  grunt.registerTask('default', ['watch', 'less', 'concat', 'uglify']);
  grunt.registerTask('js', ['concat', 'uglify']);
  grunt.registerTask('deploy', ['less', 'concat', 'uglify']);
};